import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  gaming;
  event = {timeStarts: ''};

  constructor(public navCtrl: NavController) {

  }

}
